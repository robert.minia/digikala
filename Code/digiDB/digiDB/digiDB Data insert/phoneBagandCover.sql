set identity_insert PhoneBagAndCover on;
insert into PhoneBagAndCover (PhoneBagCoverID, PhoneBagCover, phoneBagCoverFeature, phoneBagCoverStructure) values (1, 'Zoledronic Acid', 'switch', 'Aluminum');
insert into PhoneBagAndCover (PhoneBagCoverID, PhoneBagCover, phoneBagCoverFeature, phoneBagCoverStructure) values (2, 'Voltaren', 'jcb', 'Plexiglass');
insert into PhoneBagAndCover (PhoneBagCoverID, PhoneBagCover, phoneBagCoverFeature, phoneBagCoverStructure) values (3, 'Rasuvo', 'diners-club-enroute', 'Rubber');
insert into PhoneBagAndCover (PhoneBagCoverID, PhoneBagCover, phoneBagCoverFeature, phoneBagCoverStructure) values (4, 'Zolpidem Tartrate', 'jcb', 'Steel');
insert into PhoneBagAndCover (PhoneBagCoverID, PhoneBagCover, phoneBagCoverFeature, phoneBagCoverStructure) values (5, 'LISINOPRIL AND HYDROCHLOROTHIAZIDE', 'jcb', 'Brass');
insert into PhoneBagAndCover (PhoneBagCoverID, PhoneBagCover, phoneBagCoverFeature, phoneBagCoverStructure) values (6, 'Axe', 'jcb', 'Glass');
insert into PhoneBagAndCover (PhoneBagCoverID, PhoneBagCover, phoneBagCoverFeature, phoneBagCoverStructure) values (7, 'Famciclovir', 'jcb', 'Plexiglass');
insert into PhoneBagAndCover (PhoneBagCoverID, PhoneBagCover, phoneBagCoverFeature, phoneBagCoverStructure) values (8, 'Fentanyl - NOVAPLUS', 'diners-club-us-ca', 'Steel');
insert into PhoneBagAndCover (PhoneBagCoverID, PhoneBagCover, phoneBagCoverFeature, phoneBagCoverStructure) values (9, 'Prilosec OTC', 'jcb', 'Steel');
insert into PhoneBagAndCover (PhoneBagCoverID, PhoneBagCover, phoneBagCoverFeature, phoneBagCoverStructure) values (10, 'Atenolol', 'jcb', 'Steel');
insert into PhoneBagAndCover (PhoneBagCoverID, PhoneBagCover, phoneBagCoverFeature, phoneBagCoverStructure) values (11, 'Warfarin Sodium', 'visa', 'Steel');
insert into PhoneBagAndCover (PhoneBagCoverID, PhoneBagCover, phoneBagCoverFeature, phoneBagCoverStructure) values (12, 'Mobic', 'jcb', 'Steel');
insert into PhoneBagAndCover (PhoneBagCoverID, PhoneBagCover, phoneBagCoverFeature, phoneBagCoverStructure) values (13, 'Good Sense Pain Relief', 'jcb', 'Glass');


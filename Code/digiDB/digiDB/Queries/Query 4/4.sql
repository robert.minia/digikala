USE [digiDB]
GO
/****** Object:  StoredProcedure [dbo].[AveragePriceOfMonitor]    Script Date: 1/24/2023 9:30:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[AveragePriceOfMonitor] as 
begin 
	select AVG(monitorPrice)
	from Monitor as mn
	where mn.monitorScreenSize > 27 and mn.monitorPanelType='Gplus'
end 

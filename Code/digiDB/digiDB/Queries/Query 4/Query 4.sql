create procedure AveragePriceOfMonitor as 
begin 
	select AVG(monitorPrice)
	from Monitor as mn
	where mn.monitorScreenSize > 27 and mn.monitorPanelType='Gplus'
end 

exec AveragePriceOfMonitor


-- select *
-- from Monitor as mn
-- where mn.monitorScreenSize > 27 and mn.monitorPanelType='Gplus'
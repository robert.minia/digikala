CREATE TABLE PowerBank(
	powerBankID int IDENTITY PRIMARY KEY , 
	PhoneBagCover varchar(255) , 
	powerBankFeature varchar(255) ,
	powerBankStracture varchar(255) , 
	powerBankCompatiblebyMobile varchar(255) , 
	FOREIGN KEY (digitalGoodsID) REFERENCES digitalGoods(digitalGoodsID),
);
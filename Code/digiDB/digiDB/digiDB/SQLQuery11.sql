CREATE TABLE Monitorr(
	monitorID int IDENTITY PRIMARY KEY , 
	monitorCountofPort int,
	monitorScreenSize int not null , 
	monitorPanelType varchar(255) , 
	monitorResponseTimeRange varchar(255) , 
	monitorBackgroundType varchar(255) , 
	monitorType varchar(255) , 
	monitorLightBackground varchar(255) , 
	monitorPrice int ,
);

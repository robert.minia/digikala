CREATE TABLE KeyBoard(
	KeybordID int IDENTITY primary key , 
	CableLength float not null , 
	ConnectorType varchar(255) , 
	OtherFeature varchar , 
	NumberOfHotkey int , 
	AccuracyRange float  , 
	NumberofKey int not null, 
	BoardKey varchar(30),
	FOREIGN KEY (digitalGoodsID) REFERENCES digitalGoods(digitalGoodsID),
);
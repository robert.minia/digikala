CREATE TABLE SupportBasePhone(
	supportBasePhoneID int IDENTITY PRIMARY KEY , 
	supportBaseCover varchar(255) , 
	supportBaseFeature varchar(255) , 
	FOREIGN KEY (digitalGoodsID) REFERENCES digitalGoods(digitalGoodsID),
);
CREATE TABLE Cart(
	CartID int IDENTITY primary key ,
	CartStatus varchar(255),
	TotalPrice int ,
	FOREIGN KEY (userID) REFERENCES userSiteDigi(userID),
);
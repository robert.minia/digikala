USE [digiDB]
GO

/****** Object:  Table [dbo].[userSiteDigi]    Script Date: 1/19/2023 5:05:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[userSiteDigi](
	[userID] [int] IDENTITY(1,1) NOT NULL,
	[userName] [varchar](255) NULL,
	[userLastName] [varchar](255) NULL,
	[userEmail] [varchar](255) NULL,
	[userPhoneNumber] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO



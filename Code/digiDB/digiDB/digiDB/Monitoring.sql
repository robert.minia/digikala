USE [digiDB]
GO

/****** Object:  Table [dbo].[Monitor]    Script Date: 1/24/2023 8:54:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Monitor](
	[monitorID] [int] IDENTITY(1,1) NOT NULL,
	[monitorCountofPort] [int] NULL,
	[monitorScreenSize] [int] NOT NULL,
	[monitorPanelType] [varchar](255) NULL,
	[monitorResponseTimeRange] [varchar](255) NULL,
	[monitorBackgroundType] [varchar](255) NULL,
	[monitorType] [varchar](255) NULL,
	[monitorLightBackground] [varchar](255) NULL,
	[monitorCost] int NULL,
PRIMARY KEY CLUSTERED 
(
	[monitorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO



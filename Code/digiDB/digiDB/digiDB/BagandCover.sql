CREATE TABLE PhoneBagAndCover(
	PhoneBagCoverID int IDENTITY PRIMARY KEY , 
	PhoneBagCover varchar(255) , 
	phoneBagCoverFeature varchar(255) ,
	phoneBagCoverStructure varchar(255) , 
	FOREIGN KEY (digitalGoodsID) REFERENCES digitalGoods(digitalGoodsID),
);
CREATE TABLE Laptop(
	laptopID int IDENTITY PRIMARY KEY , 
	laptopProccessor varchar(255) ,
	laptopTypeMemoryRam varchar(255) , 
	laptopCategory varchar(20) NOT NULL CHECK (laptopCategory IN ('special for game' , 'moltyData' , 'publicMember' )),
	laptopScreenSize float , 
	laptopScreenAccuracy varchar(30) , 
	laptopGPuMmanufacturer varchar(255),
	FOREIGN KEY (digitalGoodsID) REFERENCES digitalGoods(digitalGoodsID),
);
CREATE TABLE ExternalHard(
	ExternalHardID int IDENTITY primary key , 
	Capacity varchar(255) , 
	ConnectionType varchar(255) ,
	CommunicationPorts varchar(255) , 
	FOREIGN KEY (digitalGoodsID) REFERENCES digitalGoods(digitalGoodsID),
);
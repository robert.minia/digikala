CREATE TABLE specialGameEquipment(
	specialGameEquipmentID int IDENTITY PRIMARY KEY , 
	specialGameEquipmentConnector varchar(255) ,
	specialGameEquipmentConnectorFeaturesGame varchar(255) , 
	FOREIGN KEY (digitalGoodsID) REFERENCES digitalGoods(digitalGoodsID),
);
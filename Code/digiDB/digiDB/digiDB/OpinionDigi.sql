CREATE TABLE Opinion(
	opinionID int IDENTITY PRIMARY KEY , 
	opinionDate varchar(8),
	opinionType varchar(255) , 
	opinionSubject varchar(255) ,
	opinionPossitiveNote varchar(255) , 
	opinionNegativeNote varchar(255), 
	opinionText varchar(255) , 
	FOREIGN KEY (userID) REFERENCES userSiteDigi(userID),
)



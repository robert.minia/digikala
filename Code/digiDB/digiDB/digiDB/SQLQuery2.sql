CREATE TABLE digitalGoods(
	DgoodsID int identity primary key , 
	DgoodsType varchar(255) , 
	DgoodsOtherOption varchar(255) , 
	DgoodsBrand varchar(255) , 
	DgoodsName varchar(255) , 
	DgoodsSubject varchar(255) , 
	DgoodsColor varchar(255) , 
	DgoodsCost varchar(255) , 
	DgoodsCheckExisting varchar(255) , 
	DgoodsSendSeller varchar(255) , 
	DgoodsCountProductSell int , 
);
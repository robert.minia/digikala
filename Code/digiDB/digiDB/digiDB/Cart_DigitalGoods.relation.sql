CREATE Table CartDigitalGoodsRelation {
  CartDigitalGoodsRelationID int IDENTITY PRIMARY KEY ,
	FOREIGN KEY (CartID) REFERENCES Cart(CartID),
	FOREIGN KEY (digitalGoodsID) REFERENCES digitalGoods(digitalGoodsID),
}